// Manipulations on a project

package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type Project struct {
	ProjectID          int64     `json:"project_id"`   // unique
	ProjectName        string    `json:"project_name"` // unique
	ProjectDescription string    `json:"project_description"`
	CreatedBy          string    `json:"created_by"`
	CreatedAt          time.Time `json:"created_at"`
	LastUpdatedBy      string    `json:"last_updated_by"`
	LastUpdatedAt      time.Time `json:"last_updated_at"`
}

func createProject(c *gin.Context) {
	var p Project
	if err := c.BindJSON(&p); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "invalid json",
		})
	}
	c.JSON(201, gin.H{
		"status":     "created",
		"project_id": 1,
	})
}
