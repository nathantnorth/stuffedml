package main

import (
	"database/sql"
)

type DatabaseCtx struct {
	conn *sql.Conn
}

func NewDBCtx() (DatabaseCtx, error) {
	return DatabaseCtx{}, nil
}
