// Server for the StuffedML registry

package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	gin.ForceConsoleColor()

	// Create a default router
	r := gin.Default()

	// Create a project group
	project := r.Group("/project")
	{
		project.POST("/", createProject)
	}

	r.Run(":8000")
}
